import React, {useMemo, useState, useEffect} from 'react';
import Table from './table';
import axios from 'axios';


const TableDetails = (props) => {
    // Custom component to render Genres 
const Genres = ({ values }) => {
    // Loop through the array and create a badge-like component instead of a comma-separated string
    return (
      <>
        {values.map((genre, idx) => {
          return (
            <span key={idx} className="badge">
              {genre}
            </span>
          );
        })}
      </>
    );
  };
   /* 
    - Columns is a simple array right now, but it will contain some logic later on. It is recommended by react-table to memoize the columns data
    - Here in this example, we have grouped our columns into two headers. react-table is flexible enough to create grouped table headers
  */
    const columns = useMemo(
        () => [
          {
            Header: "Details",
            columns: [
              {
                Header: "Language",
                accessor: "show.language"
              },
              {
                Header: "Genre(s)",
                accessor: "show.genres",
                // Cell method will provide the cell value; we pass it to render a custom component
                Cell: ({ cell: { value } }) => <Genres values={value} />
              },
              {
                Header: "Runtime",
                accessor: "show.runtime",
                // Cell method will provide the value of the cell; we can create a custom element for the Cell        
                Cell: ({ cell: { value } }) => {
                  const hour = Math.floor(value / 60);
                  const min = Math.floor(value % 60);
                  return (
                    <>
                      {hour > 0 ? `${hour} hr${hour > 1 ? "s" : ""} ` : ""}
                      {min > 0 ? `${min} min${min > 1 ? "s" : ""}` : ""}
                    </>
                  );
                }
              },
              {
                Header: "Status",
                accessor: "show.status"
              }
            ]
          }
        ],
        []
      );
     // data state to store the TV Maze API data. Its initial value is an empty array
     const [data, setData] = useState([]);
  
     // Using useEffect to call the API once mounted and set the data
     useEffect(() => {
       (async () => {
         const result = await axios("https://api.tvmaze.com/search/shows?q=snow");
         setData(result.data);
       })();
     }, []);

    return(
        <div>
            <Table columns={columns} data={data} />
        </div>
    )
}

export default TableDetails;