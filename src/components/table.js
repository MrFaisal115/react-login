import React, { useState, useFilters } from "react";
import { useTable } from "react-table";

export default function Table({ columns, data }) {
  
    // Create a state
    const [filterInput, setFilterInput] = useState("");

    //update the state when input changes
    const handleFilterChange = e => {
        const value = e.target.value || undefined;
        setFilter("show.name", value);
        setFilterInput(value);
    };

    //Input element
    <input value={filterInput} onChange={handleFilterChange} placeholder={"Search name"} />

    // Table component logic and UI come here
    // Use the useTable Hook to send the columns and data to build the table
    const {
        getTableProps, //table props from react-table 
        getTableBodyProps, // table body props from react-table
        headerGroups, // headerGroups, if your table has groupings
        rows, // rows for the table based on the data passed
        prepareRow, // Prepare the row (this function needs to be called for each row before getting the row props)
        setFilter
    } = useTable ({
        columns,
        data
    },
    useFilters
    );

    /* 
    Render the UI for your table
    - react-table doesn't have UI, it's headless. We just need to put the react-table props from the Hooks, and it will do its magic automatically
  */

  return (
      <table { ...getTableProps()} className="table">
        <thead>
            {headerGroups.map(headerGroups => (
                <tr { ...headerGroups.getHeaderGroupProps()}>
                    { headerGroups.headers.map(column => (
                        <th {...column.getHeaderProps()}>
                            {column.render("Header")}
                        </th>
                    ))}
                </tr>
            ))}
        </thead>
        <tbody {...getTableBodyProps()}>
            {rows.map((row, i) => {
                prepareRow(row);
                return (
                    <tr {...row.getRowProps()}>
                        {row.cells.map(cell => {
                            return <td {...cell.getCellProps()}>
                                {cell.render("Cell")}
                            </td>
                        })}
                    </tr>
                );
            })}
        </tbody>
      </table>
  );


}