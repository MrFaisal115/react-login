import React from 'react';

export const Contact = (props) => {
    return(
    <div className='contact'>
      <h1>Contact</h1>
      
        <div className="form-group">
            <label htmlFor="name">First Name:</label>
            <input className="form-control" type="text" name="firstName" id="firstname"  />
        </div>
        <div className="form-group">
            <label htmlFor="name">Last Name:</label>
            <input className="form-control" type="text" name="lastName" id="lastname"  />
        </div>
        <div className="form-group">
            <label htmlFor="email">Email:</label>
            <input className="form-control" type="email" name="email" id="email"  />
        </div>
        <div className="form-group">
            <label htmlFor="messsage">Message:</label>
            <textarea className="form-control" type="text" name="messsage" id="messsage"  ></textarea>
        </div>
        <div className="form-group">
            <input className="btn btn-primary" type="button" name="submit" id="submit" value="Submit" />
        </div>


    </div>
    )
}

export default Contact;