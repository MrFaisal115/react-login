import { NavLink, Switch, Route } from 'react-router-dom';
import Home from './home';
import About from './about';
import Contact from './contact';
import TableDetails from './tableDetails';

const Main = () => (
    <Switch>
      <Route exact path='/' component={Home}></Route>
      <Route exact path='/about' component={About}></Route>
      <Route exact path='/contact' component={Contact}></Route>
      <Route exact path='/tableDetails' component={TableDetails}></Route>

    </Switch>
  );

  export default Main;