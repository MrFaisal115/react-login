import React, { useMemo, useState, useEffect } from 'react';
import LoginForm from './components/LoginForm';
import { Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

import Main from './components/main';
import Navigation from './components/navigation';


function App() {
  

//User login code goes here
  const adminUser = {
    email: "admin@admin.com",
    password: "123"
  }

  const [user, setUser] = useState({name: "", email: ""});
  const [error, setError] = useState("");

  const Login = details => {
    console.log(details);

    if(details.email == adminUser.email && details.password == adminUser.password){
      console.log("Logged In");
      setUser({
        name: details.name,
        email: details.email
      });
    }else{
      console.log("Details do no match!");
      setError("Details do no match!");
    }
  }

  const Logout = () => {

    setUser({ name: "", email: ""});
  }

  return (
    <div className="App">
      
      {(user.email != "") ? (
        <div className="welcome">
          <Navigation />
          <Main />
          

          <h2>Welcome, <span>{user.name}</span></h2>
          <button onClick={Logout}>Logout</button>
        </div>
      ) : (
        <LoginForm Login={Login} error={error} />
      )}
    </div>
  );
}

export default App;
